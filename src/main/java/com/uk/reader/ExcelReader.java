package com.uk.reader;



import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class ExcelReader {

    public static String FILELOCATION = "/Users/arahant.yalagudri/Desktop/DataSheet.xlsx";


    private static FileInputStream fis;
    private static XSSFWorkbook wb;
    private static XSSFSheet sh;
    private static XSSFRow row;

    public static void loadExcel() throws Exception {

        File file = new File(FILELOCATION);
        fis = new FileInputStream(file);
        wb = new XSSFWorkbook(fis);
        sh = wb.getSheet("TestData");

    }


    public static List<Map<String, String>> readData() throws Exception {

        String scenarioID ="Scenario 1";
        if (sh == null) {
            loadExcel();

        }

        List<Map<String, String>> mylst = new ArrayList<>();


        // int rows = sh.getLastRowNum();
        row = sh.getRow(0);
        for (int j = 1; j < row.getLastCellNum(); j++) {
            Map<String, String> myMap = new HashMap<>();


            if(row.getCell(j).getStringCellValue().equals(scenarioID)){

                for (int i = 1; i < sh.getLastRowNum(); i++) {
                    Row r = CellUtil.getRow(i, sh);
                    String keys = CellUtil.getCell(r, 0).getStringCellValue();
                    String values = CellUtil.getCell(r, j).getStringCellValue();
                    myMap.put(keys, values);
                }   mylst.add(myMap);

            }


        }
        return mylst;

       // Map<String,String> map = new Map <String, String>;
       // map = mylst.stream().collect(Collectors.toMap(i)).

    }




    public  static void main(String[] args) throws Exception {

       // Map<String,String> map = new Map <String, String>;
       // map = li


    }

}