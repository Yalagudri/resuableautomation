package com.uk.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
        private Properties properties;
        private String path = "config//ApplicationConfig.properties";
        public ConfigReader()
        {
            BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader(path));
                properties = new Properties();
                try {
                   properties.load(reader);
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException("ApplicationConfig.properties not found at " + path);
            }
        }

        public String getApplicationURL()
        {
            String URL = properties.getProperty("appURL");
            if(URL!= null)
                return URL;
            else
                throw new RuntimeException("Application URL not specified in the ApplicationConfig.properties file.");
        }

        public String getChromeDriverPath(){
            String path = properties.getProperty("chromeDriverpath");
            if(path!= null) {
                String osname = System.getProperty("os.name").toLowerCase();
                if(osname.contains("mac"))
                    return path;
                else
                    return path+".exe";
            }
            else
                throw new RuntimeException("Chrome driver path not specified in the ApplicationConfig.properties file.");
        }

        public String getGeckoDriverPath(){
            String path = properties.getProperty("geckoDriverpath");
            if(path!= null)
            {
                String osname = System.getProperty("os.name").toLowerCase();
                if(osname.contains("mac"))
                    return path;
                else
                    return path+".exe";
            }
            else
                throw new RuntimeException("Gecko driver path not specified in the ApplicationConfig.properties file.");
        }


        public String getBrowserName()
        {

            String browserName = properties.getProperty("browserName");
            if(browserName!= null)
                return browserName;
            else
                return "";

        }
    }

