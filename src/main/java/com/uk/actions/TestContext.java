package com.uk.actions;

import com.uk.context.ScenarioContext;
import com.uk.reader.ExcelReader;
import com.uk.utility.Log;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import cucumber.api.Scenario;
import org.apache.log4j.Logger;

import java.net.MalformedURLException;

public class TestContext {
    private Logger log = Log.getLogger(TestContext.class);
    private WebDriver driver;
    private BrowserFactory browserFactory;
    public Scenario scenario;
    public ScenarioContext scenarioContext;
    public ExcelReader excelreader;

    @Before
    public void setUp(Scenario scenario) {
        log.info("=================" + scenario.getName() + " execution starts" + "===================");
        this.scenario = scenario;
      //  excelreader = new ExcelReader("", "", "");
        scenarioContext = new ScenarioContext();
    }
    @Given("user logs in to the application \"([^\"]*)\"$")
    public void userLogsInToTheApplication( String scenarioID) throws MalformedURLException {
       // scenarioContext.setContext(jsonUtilityObj.convertJSONtoMAP(scenarioID));
        browserFactory = new BrowserFactory();
        browserFactory.initiateDriver();
        driver = browserFactory.getDriver();


//        objectManager = new PageObjectManager(driver);
//        browserFactory.launchURL();
//        loginPageObj = objectManager.getLoginPageObj();
//        loginPageObj.enterText("Username", scenarioContext.getContext("Username"));
//        loginPageObj.enterText("Password", scenarioContext.getContext("Password"));
//        loginPageObj.enterText("Token Security Code", loginPageObj.generateSecurityToken(scenarioContext.getContext("SecretKey")));
//        loginPageObj.clickButton("Login");
//        dashboardPageObj = objectManager.getDashboardPageObj();
//        dashboardPageObj.verifyDashboardWelcomeMsg();

    }





    @After
    public void cleanUp() throws Exception {
        if (scenario.isFailed()) {
            log.error("Scenario failed. Screenshot attached to report.");
        }

        log.info("=================" + scenario.getName() + " execution ends" + "===================");

        if (driver != null) {
            driver.quit();
            driver = null;
        }

        if (excelreader != null) {
            excelreader = null;
        }

        if (scenarioContext != null) {
            scenarioContext.clearContext();
        }
    }
}
