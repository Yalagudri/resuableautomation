package com.uk.actions;
import com.uk.reader.ConfigReader;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;



import java.net.MalformedURLException;
import java.net.URL;

public class BrowserFactory {

    private WebDriver driver;
    private ConfigReader configReader;


    //Saucelab username
    public static final String USERNAME = "currencypay";
    //Access key: you will get it from soucelab: Go to My Accounts -> Scroll till the end
    public static final String ACCESS_KEY = "e43a9713-4a7d-4f5d-9c44-07cf77f57ced";
    public static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
    DesiredCapabilities caps = null;

    public WebDriver initiateDriver() throws MalformedURLException {
        configReader = new ConfigReader();

        switch(configReader.getBrowserName())
        {
            case "FIREFOX":
                System.setProperty("webdriver.gecko.driver", configReader.getGeckoDriverPath());
               // driver = new FirefoxDriver();
                driver.manage().window().maximize();
                break;
            case "CHROME":
                System.setProperty("webdriver.chrome.driver", configReader.getChromeDriverPath());
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                break;
            case "SAFARI":
               // driver = new SafariDriver();
                driver.manage().window().maximize();
                break;
            case "SL_WIN_CHROME":
                caps = DesiredCapabilities.chrome();
                caps.setCapability("platform", "Windows 10");
                caps.setCapability("version", "71.0");
                caps.setCapability("recordVideo", "false");
                caps.setCapability("screenResolution", "1600x1200");
                driver = new RemoteWebDriver(new URL(URL), caps);
                driver.manage().window().maximize();
                break;
            case "SL_WIN_FIREFOX":
                caps = DesiredCapabilities.firefox();
                caps.setCapability("platform", "Windows 10");
                caps.setCapability("version", "50.0");
                driver = new RemoteWebDriver(new URL(URL), caps);
                driver.manage().window().maximize();
                break;
            case "SL_WIN_EDGE":
                caps = DesiredCapabilities.edge();
                caps.setCapability("platform", "Windows 10");
                caps.setCapability("version", "18.17763");
                driver = new RemoteWebDriver(new URL(URL), caps);
                driver.manage().window().maximize();
                break;
            case "SL_MAC_SAFARI":
                caps = DesiredCapabilities.safari();
                caps.setCapability("platform", "macOS 10.14");
                caps.setCapability("version", "12.0");
                driver = new RemoteWebDriver(new URL(URL), caps);
                driver.manage().window().maximize();
                break;
            case "SL_MAC_CHROME":
                caps = DesiredCapabilities.chrome();
                caps.setCapability("platform", "macOS 10.14");
                caps.setCapability("version", "71.0");
                driver = new RemoteWebDriver(new URL(URL), caps);
                driver.manage().window().maximize();
                break;
            case "SL_ANDROID_CHROME":
                caps = DesiredCapabilities.android();
                caps.setCapability("appiumVersion", "1.8.1");
                caps.setCapability("deviceName","Samsung Galaxy S9 Plus HD GoogleAPI Emulator");
                caps.setCapability("deviceOrientation", "portrait");
                caps.setCapability("browserName", "Chrome");
                caps.setCapability("platformVersion", "7.1");
                caps.setCapability("platformName","Android");
              //  driver = new AndroidDriver(new URL(URL), caps);
                break;
            case "SL_IPHONE_SAFARI":
                caps = DesiredCapabilities.iphone();
                caps.setCapability("appiumVersion", "1.8.1");
                caps.setCapability("deviceName","iPhone X Simulator");
                caps.setCapability("deviceOrientation", "portrait");
                caps.setCapability("platformVersion","11.3");
                caps.setCapability("platformName", "iOS");
                caps.setCapability("browserName", "Safari");
              //  driver = new AppiumDriver(new URL(URL), caps);
                break;
            case "SL_REAL_IPHONE6_SAFARI":
                caps = DesiredCapabilities.iphone();
                caps.setCapability("testobject_api_key", "F7BA20BB15F54AA6AC339E56B81D18A4");
                caps.setCapability("deviceName","iPhone 6");
                caps.setCapability("platformVersion","12.1.2");
                caps.setCapability("platformName", "iOS");
                caps.setCapability("browserName", "Safari");
               // driver = new AppiumDriver(new URL("https://eu1.appium.testobject.com/wd/hub"), caps);
                break;
            case "SL_REAL_ONEPLUS6_CHROME":
                caps = DesiredCapabilities.android();
                caps.setCapability("testobject_api_key", "F7BA20BB15F54AA6AC339E56B81D18A4");
                caps.setCapability("deviceName","OnePlus 6");
                caps.setCapability("platformVersion","9");
                caps.setCapability("platformName", "Android");
                caps.setCapability("browserName", "Chrome");
              //  driver = new AppiumDriver(new URL("https://eu1.appium.testobject.com/wd/hub"), caps);
                break;
            case "CHROME_HEADLESS":
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                options.addArguments("--whitelisted-ips");
                options.addArguments("--no-sandbox");
                options.addArguments("--disable-extensions");
                driver = new ChromeDriver();
                driver.manage().window().maximize();

        }

        return driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void launchURL(String URL) {
        driver.get(URL);
       // log.info(URL+" launched");
    }

    public void launchURL()
    {
        driver.get(configReader.getApplicationURL());
    }
}