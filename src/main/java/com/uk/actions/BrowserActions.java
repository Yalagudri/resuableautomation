package com.uk.actions;

import cucumber.api.Scenario;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.interactions.Actions;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class BrowserActions {

    private WebDriver driver;
    private WebDriverWait wait;
    private Scenario scenario;

    public void enterText(String fieldName, String value) {
        String Xpath = ".//*[contains(text(),'" + fieldName + "')]//..//input";
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Xpath)));
        element.sendKeys(value);
//    waitForSeconds(1);
        element.sendKeys(Keys.TAB);
    }


    public void clickButton(String fieldName) {

        String Xpath = ".//*[contains(text(),'\" + fieldName + \"')]";
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Xpath)));
        clickElement(element);

    }

    public void clearTextBox(WebElement element) {
        Actions actions = new Actions(driver);
        int textLength = element.getAttribute("value").length();
        while (textLength > 0) {
            actions.moveToElement(element)
                    .click()
                    .sendKeys(Keys.BACK_SPACE)
                    .build()
                    .perform();
            textLength--;
        }
    }

    public boolean isElementExists(String element) {

        try {

            driver.findElement(By.xpath(element));
            return true;
        } catch (NoSuchElementException e) {

        }
        return false;

    }

    public void waitForElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForElementToBeVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void clickElement(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void waitForLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(pageLoadCondition);
    }

    public void takeSnapShot() {
        TakesScreenshot ts = (TakesScreenshot) driver;
        byte[] source = ts.getScreenshotAs(OutputType.BYTES);
        scenario.embed(source, "image/png");
    }

    public void selectAccountViaAccNum(String fieldName, String accountNumber) {
        //waitForSeconds(1);
        WebElement arrowElement = driver.findElement(By.xpath(".//*[contains(text(),'" + fieldName + "')]//..//span"));
        waitForElementToBeClickable(arrowElement);
        clickElement(arrowElement);
        // waitForSeconds(1);
        String XPATH = ".//*[contains(text(),'" + fieldName + "')]//..//div[contains(@class,'Select-menu')]/div/div/div[contains(@class,'Account')]";
        List<WebElement> list = driver.findElements(By.xpath(XPATH));
        Iterator<WebElement> i = list.iterator();
        while (i.hasNext()) {
            WebElement element = i.next();
            if (element.getText().contains(accountNumber)) {
                waitForElementToBeClickable(element);
                clickElement(element);
                break;
            }
        }

    }
}
